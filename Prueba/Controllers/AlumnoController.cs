﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PYVA.Interfaces;
using PYVA.Models;
using PYVA.Repositories;


namespace Prueba.Controllers
{
    public class AlumnoController : Controller
    {
        //
        // GET: /Alumno/

        //private AlumnoInterface repository;

        private PVYAContext repository;
        


        public AlumnoController()
        {
            repository = new PVYAContext();
        }

        //public AlumnoController(AlumnoInterface repository)
        //{
        //    this.repository = repository;
        //}

        public ViewResult Create()
        {
            var alumno = repository.AllAlumnos();
            ViewData["Id"] = new SelectList(alumno, "Id", "Nombre", "Apellidos");
            return View("Create");
        }

        
        public ViewResult Details(int id)
        {
            var data = repository.GetAlumnoId(id);
            return View("Details", data);
        }

        //public ViewResult Index()
        //{
        //    var ser = new AlumnoInterface();
        //    var alumno = ser.AllAlumnos();

        //    return View("Inicio", alumno);
        //}

        //public ViewResult Detalle(int id)
        //{
        //    var persona = repository.GetAlumnoId(id);
        //    return View("Detalle", persona);
        //}

       
    }
}
