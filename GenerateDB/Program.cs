﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PYVA.Models;
using PYVA.Repositories;

namespace GenerateDB
{
    class Program
    {
        static void Main(string[] args)
        {
            var alumno = new Alumno()
            {
                Nombre="Andres",
                Apellidos="Pariona"
            };

            var context = new PVYAContext();
            Console.WriteLine("Creando Base de Datos");

            context.Alumnos.Add(alumno);

            context.SaveChanges();

            Console.WriteLine("Base de datos Creada OK!!");
            Console.ReadLine();
        }
    }
}
