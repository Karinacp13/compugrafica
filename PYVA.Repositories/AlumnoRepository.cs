﻿using PYVA.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PYVA.Repositories;
using PYVA.Models;

namespace PYVA.Repositories
{
    public class AlumnoRepository:AlumnoInterface
    {
        PVYAContext entities;
        public AlumnoRepository(PVYAContext entities)
        {
            this.entities = entities;
        }

        public List<Alumno> AllAlumnos()
        {
            var result = from p in entities.Alumnos select p;
            return result.ToList();
        }
        public Alumno GetAlumnoId(int id)
        {
            Alumno resultado = new Alumno();
            using (PVYAContext ctx = new PVYAContext())
            {
                var oct = ctx.Alumnos.Where(p => (p.Id == id));
                resultado = oct.First();

            }
            return resultado;
        }

    }
}
