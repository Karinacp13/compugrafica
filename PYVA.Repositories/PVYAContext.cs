﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PYVA.Models;
using PYVA.Repositories;


namespace PYVA.Repositories
{
    public class PVYAContext : DbContext
    {
        public PVYAContext()
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<PVYAContext>());
        }

        public virtual IDbSet<Alumno> Alumnos { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new AlumnoMap());
        }
    }
}
