﻿using PYVA.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PYVA.Repositories
{
    public class AlumnoMap : EntityTypeConfiguration<Alumno>
    {
        public AlumnoMap(){
            this.HasKey(p => p.Id);
            this.Property(p => p.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(p => p.Nombre)
                .HasMaxLength(200)
                .IsRequired();

            this.Property(p => p.Apellidos)
                .HasMaxLength(200)
                .IsRequired();

            this.ToTable("Alumno");
        }
    }
}
