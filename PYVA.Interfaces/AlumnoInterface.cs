﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PYVA.Models;


namespace PYVA.Interfaces
{
    public interface AlumnoInterface
    {
        List<Alumno> AllAlumnos();
        Alumno GetAlumnoId(int id);
    }
}
